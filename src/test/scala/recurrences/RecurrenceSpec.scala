package recurrences

import org.scalatest.{Matchers, FlatSpec}

/**
 * Created by stephane on 25/03/15.
 */
class RecurrenceSpec extends FlatSpec with Matchers {

  val recurrence = new RecurrenceProblem

  "a Fibonnaci function" should "return 0 for f0" in {
    recurrence.fibonacci(0) should be (0)
  }

  it should "compute f1 and f2" in {
    recurrence.fibonacci(1) should be (1)
    recurrence.fibonacci(2) should be (1)
  }

  it should "compute fn" in {
    recurrence.fibonacci(6) should be (8)
  }

  it should "compute f6" in {
    recurrence.fibo(6) should be (8)
  }

}
