package lists

import org.scalatest.{Matchers, FlatSpec}

/**
 * Created by stephane on 20/03/15.
 */
class ListsSpec extends FlatSpec with Matchers {

  val list = new ListProblem
  val palin = List(1,2,3,2,1)
  val l1 = List(1,2,3,4)
  val rev = List(4,3,2,1)

  "List Spec" should "reverse the list" in {
    list.reverse[Int](l1) should be (rev)
  }

  it should "find if a list is a palindrome" in {
    list.isPalindrome(palin) should be (true)
  }

  it should "flatten the list" in {
    val l = List(List(1,2), 3,List(4))
    list.flatten(l) should be (l1)
  }

  it should "duplicate each element into list" in {
    val dup = List(1,1,2,2,3,3,4,4)
    list.duplicateN(2,l1) should be (dup)
  }

  it should "drop the n element" in {
    list.drop(2,l1) should be(List(1,2,4))
  }

  it should "slice a list" in {
    list.slice(1,3,palin) should be(List(2,3,2))
  }

  it should "rotate a list" in {
    list.rotate(2, l1) should be (List(3,4,1,2))
    list.rotate(-1,l1) should be (List(4,1,2,3))
  }

  it should "remove an element from list" in {
    list.removeAt(1,l1) should be ((List(1,3,4),Some(2)))
  }

  it should "insert an element in list" in {
    list.insertAt(5,2,l1) should be (List(1,2,5,3,4))
  }

  it should "range a list" in {
    list.rangeTailRec(4,9) should be (List(4,5,6,7,8,9))
  }

  it should "select random correctly" in {
    val l = list.randomSelect(3, List(7,5,6,1,7,2))
    l.length should be (3)
  }

  it should "get 6 randoms numbers" in {
    val l = list.lotto(6,49)
    l.length should be (6)
  }

  it should "get url data" in {
    val url = "www.something.com?name=wong&firstname=stephane"
    val l = list.split(url)
    1 should be (1)
  }

}
