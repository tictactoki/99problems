package recurrences;
import org.junit.Assert;


/**
 * Created by stephane on 25/03/15.
 */
public class RecurrencesJavaTest {

    private RecurrenceProblemJava recurrence = new RecurrenceProblemJava();


    public void fibonacciCheck() {
        int fibo = recurrence.fibonacci(5);
        int secondFibo = recurrence.secondFibo(5);
        Assert.assertEquals(8,fibo);
        Assert.assertEquals(8,secondFibo);
    }

}
