package lists

import scala.annotation.tailrec
import scala.util.Random

/**
 * Created by stephane on 20/03/15.
 */
class ListProblem {

  def reverse[T](list: List[T]): List[T] = list match {
    case (Nil) => Nil
    case (h :: t) => reverse(t) ::: List(h)
  }


  def isPalindrome[T](list: List[T]): Boolean = list == reverse(list)

  def flatten(list: List[Any]): List[Any] = list.flatMap {
    case l: List[_] => flatten(l)
    case e => List(e)
  }

  def compress[T](list: List[T]): List[T] = {
    list.foldRight(List[T]()) { (v, l) =>
      if (l.isEmpty || l.head != v) v :: l
      else l
    }
  }

  def pack[T](list: List[T]): List[List[T]] = {
    if (list.isEmpty) List(List())
    else {
      val (take, drop) = list.span(_ == list.head)
      if (drop.isEmpty) List(take)
      else take :: pack(drop)
    }
  }

  def encode[T](list: List[T]): List[(Int, T)] = {
    pack(list).map { elt => (elt.length, elt.head)}
  }

  def decode[T](list: List[(Int, T)]): List[T] = {
    list.flatMap { elt => List.fill(elt._1)(elt._2)}
  }

  def duplicateN[T](n: Int, list: List[T]): List[T] = {
    list.flatMap { elt => List.fill(n)(elt)}
  }

  def drop[T](n: Int, list: List[T]): List[T] = {
    val take = list.take(n)
    val dro = list.drop(n + 1)
    take ::: dro
  }

  def slice[T](i: Int, k: Int, list: List[T]): List[T] = {
    list.drop(i).take(k)
  }

  def rotate[T](n: Int, list: List[T]): List[T] = {
    val abs = Math.abs(n)
    if (abs > list.size) list
    else if (n <= 0) {
      list.drop(list.size - abs) ::: list.take(list.size - abs)
    }
    else list.drop(abs) ::: list.take(abs)
  }

  def removeAt[T](n: Int, list: List[T]): Tuple2[List[T], Option[T]] = (list, n) match {
    case (Nil, _) => (Nil, None)
    case (h :: t, 0) => (t, Some(h))
    case (h :: t, n) => {
      val (ls, a) = removeAt(n - 1, list.tail)
      (list.head :: ls, a)
    }
  }

  def insertAt[T](elm: T, pos: Int, list: List[T]) = {
    if (pos < 0 || list.size < pos) list
    else {
      val (take, drop) = list.splitAt(pos)
      take ::: (elm :: drop)
    }
  }

  def split[T](pos: Int, list: List[T]): (List[T], List[T]) = {
    (list.take(pos), list.drop(pos))
  }

  def range(start: Int, end: Int): List[Int] = {
    List.range(start, end)
  }

  def rangeRec(start: Int, end: Int): List[Int] = {
    if (end < start) Nil
    else start :: rangeRec(start + 1, end)
  }

  def rangeTailRec(start: Int, end: Int): List[Int] = {
    def rangeR(end: Int, res: List[Int]): List[Int] = {
      if (end < start) res
      else rangeR(end - 1, end :: res)
    }
    rangeR(end, Nil)
  }

  def randomSelect[T](size: Int, list: List[T]): List[T] = {
    @tailrec
    def aux(s: Int, laux: List[T], l: List[T]): List[T] = {
      if (s <= 0) laux
      else {
        val (llaux, v) = removeAt(new Random().nextInt(l.length), l)
        aux(s - 1, v.getOrElse(throw new Exception) :: laux, llaux)
      }
    }
    aux(size, Nil, list)
  }

  def lotto(size: Int, limit: Int): List[Int] = {
    @tailrec
    def aux(size: Int, list: List[Int]): List[Int] = {
      if (size <= 0) list
      else {
        val rand = new Random().nextInt(limit)
        aux(size - 1, rand :: list)
      }
    }
    aux(size, Nil)
  }

  //type de l'url www.something.com?name=stephane&firstname=wong
  type T = Tuple2[String, String]

  def split(url: String): List[T] = {
    url.split("[?]").tail.head.split("&").toList.map { s =>
      val l = s.split("=")
      (l.head, l.tail.head)
    }
  }


}
