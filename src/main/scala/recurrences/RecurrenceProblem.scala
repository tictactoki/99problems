package recurrences

import scala.annotation.tailrec

/**
 * Created by stephane on 25/03/15.
 */
class RecurrenceProblem {

  // classic
  def fibonacci(n: Int): Int = n match {
    case 0 => 0
    case 1 | 2 => 1
    case _ => fibonacci(n - 1) + fibonacci(n - 2)
  }

  // tail rec
  def tailFibo(n: Long): Long = {
    @tailrec
    def aux(n: Long, value: Long, acc: Long = 0): Long = n match {
      case 0 => acc
      case _ => aux(n - 1, acc, value + acc)
    }
    aux(n, 1)
  }

  // verbose but we can check all result in case
  def fibo(n: Int): Int = {
    if(n <= 0) 0
    else if (n == 1 || n == 2) 1
    else {
    var array: Array[Int] = new Array[Int](n+1)
      array(0) = 0
      array(1) = 1
      array(2) = 1
      for(i <- 3 to n) {
        array(i) = array(i-1) + array(i-2)
      }
      array(n)
    }
  }


}
