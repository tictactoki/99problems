package recurrences;

/**
 * Created by stephane on 25/03/15.
 */
public class RecurrenceProblemJava {


    public int fibonacci(int n) {
        if (n <= 0) return 0;
        if (n == 1 || n == 2) return 1;
        else return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public int secondFibo(int n) {
        if (n <= 0) return 0;
        if (n == 1) return 1;
        else {
            int array[] = new int[n + 1];
            array[0] = 0;
            array[1] = array[2] = 1;
            for (int i = 3; i < n; i++) {
                array[i] = array[i - 1] + array[i - 2];
            }
            return array[n];
        }
    }


}
